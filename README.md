This is an assignment to display the dates and timeslots available for the dates so that the user can choose a slot 

## Steps to run the application on a local system

To Run the application on your local system, perform the following steps:

- Clone this repository on your local machine.
- Open command prompt / terminal.
- Navigate to the Project folder containing `package.json` file.
- Run the following command
    ```
    npm install
    ```
- Once npm install is successful, run the following command
    ```
    npm start
    ```

## Commands and their Uses

The above mentioned commands behave as mentioned below:

### `npm install`

Installs all the dependencies required for the application to run

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
