export const DATE_ACTIONS = {
    FETCH_DATES: 'FETCH_DATES',
    FETCH_DATES_SUCCESS: 'FETCH_DATES_SUCCESS',
    FETCH_DATES_FAIL: 'FETCH_DATES_FAIL',
    FETCH_TIME_SLOTS: 'FETCH_TIME_SLOTS',
    FETCH_TIME_SLOTS_SUCCESS: 'FETCH_TIME_SLOTS_SUCCESS',
    FETCH_TIME_SLOTS_FAIL: 'FETCH_TIME_SLOTS_FAIL'
}

export const fetchDeliveryDates = () => {
    return({
        type: DATE_ACTIONS.FETCH_DATES,
        payload: {
            request: {
                url: '/dates',
                method: 'GET'
            }
        }
    });
}

export const fetchTimeSlots = (dateString) => {
    return({
        type: DATE_ACTIONS.FETCH_TIME_SLOTS,
        payload: {
            request: {
                url: `/times/${dateString}`,
                method: 'GET'
            }
        }
    });
}