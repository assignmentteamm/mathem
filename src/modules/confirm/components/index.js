import React from "react";
import TimeSlotsItem from "../../timeSlots/components/timeSlotsItem";
import { generateHash } from "random-hash";
import { Button } from "react-bootstrap";
import { withRouter } from "react-router-dom";

function ConfirmScreen(props) {
    const selectedTimeSlot = JSON.parse(window.localStorage.getItem('selectedTimeSlot'));

    return (
        <div className='centreClass'>
            <div className='row'>
                <div className='col-sm-12'>
                    You have selected the following time slot:
                </div>
                <div className='col-sm-12'>
                    <div className='timeSlots grid-noSelect'>
                        <TimeSlotsItem
                            key={generateHash()}
                            timeSlot={selectedTimeSlot}
                            selectedSlot={selectedTimeSlot}
                        />
                    </div>
                </div>
                <div className='col-sm-12'>
                    Do you Wish to Confirm?
                </div>
                <div className='col-sm-12'>
                    <Button
                        variant="primary"
                        onClick={() => {
                            goBack(props)
                        }}
                    >
                        Back
                    </Button>
                    <Button
                        variant="primary"
                        onClick={() => {
                            confirm(props)
                        }}
                    >
                        Confirm
                    </Button>
                </div>
            </div>
        </div>
    );
}

const goBack = (props) => {
    const {history} = props;

    history.goBack();
}

const confirm = (props) => {
    const {history} = props;

    history.push('/success');
}

export default withRouter(ConfirmScreen);