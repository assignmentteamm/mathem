import React, { Component } from "react";
import { Form } from "react-bootstrap";
import generateHash from 'random-hash';

class DeliveryDateItem extends Component {  
    render() {
        const {
            deliveryDates,
            errorMessage,
            handleDateSelection,
            selectedDate
        } = this.props;

        const selectedValue = selectedDate || window.localStorage.getItem('selectedDate');

        return (
            <>
                <Form>
                    <Form.Group controlId="exampleForm.ControlSelect1">
                        <span>Select a Date</span>
                        <Form.Control as="select"
                            onChange={handleDateSelection}
                            value={selectedValue}
                        >
                            <option>{''}</option>
                            {deliveryDates && deliveryDates.map(date => {
                                return <option key={generateHash()}>{date}</option>
                            })}
                            {errorMessage && <option>{errorMessage}</option>}
                        </Form.Control>
                    </Form.Group>
                </Form>
            </>
        );
    }
}

export default DeliveryDateItem;