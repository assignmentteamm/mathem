import React, { Component } from "react";
import { Form } from "react-bootstrap";
import DeliveryDateItem from "./deliveryDateItem";

class DeliveryDate extends Component {
    getCheckboxValueFromLocalStorage = () => {
        const currentStatus = window.localStorage.getItem('checkboxStatus') !== null ? window.localStorage.getItem('checkboxStatus') : false;
        const result = currentStatus === 'true' ? true : false;

        return result;
    }

    checkboxClicked = () => {
        debugger;
        const {handleCheckboxChange} = this.props;
        const checkboxValue = this.getCheckboxValueFromLocalStorage();

        window.localStorage.setItem('checkboxStatus', !checkboxValue);

        handleCheckboxChange();
    }

    render() {
        const {
            deliveryDates,
            errorMessage,
            handleDateSelection,            
        } = this.props;
        const checkboxValue = this.getCheckboxValueFromLocalStorage();
        
        return (
            <>
                <div className='col-sm-12 col-md-9 dateDropdown'>
                    <DeliveryDateItem
                        deliveryDates={deliveryDates}
                        errorMessage={errorMessage}
                        handleDateSelection={handleDateSelection}
                    />
                </div>
                <div className="col-sm-12 col-md-3 dateCheckBox">
                    <Form.Group controlId="formBasicCheckbox" >
                        <Form.Check
                            type="checkbox"
                            label="Show only Home Delivery Slots"
                            checked={checkboxValue}
                            onChange={this.checkboxClicked}
                        />
                    </Form.Group>
                </div>
            </>
        );
    }
}

export default DeliveryDate;