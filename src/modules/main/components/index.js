import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "react-bootstrap";
import { withRouter } from "react-router-dom";

import DeliveryDate from "../../date/components";
import TimeSlotsComponent from "../../timeSlots/components";
import { fetchDeliveryDates, fetchTimeSlots } from "../../actions";
import '../../styles/timeSlots.css'

class MainComponent extends Component {
    state = {
        checkboxChecked: false,
        timeSlotArray: [],
        filteredTimeSlotArray: [],
        buttondisabled: true,
        clearSelectedSlot: false,
        selectedDate:''
    }

    static getDerivedStateFromProps(props, state) {
        if(props.timeSlots !== state.timeSlotArray){
            return {
                timeSlotArray: props.timeSlots
            }
        }
        else{
            return({
                timeSlotArray: state.timeSlotArray
            })
        }
    }

    componentDidMount() {
        const { fetchDeliveryDates } = this.props;

        fetchDeliveryDates();
    }

    componentDidUpdate() {
        const { timeSlots } = this.props;
        const slotStoredInLocalStorage = JSON.parse(window.localStorage.getItem('allTimeSlots'));
        let effectiveTimeSlots = [];

        if(timeSlots && timeSlots.length > 0){
            effectiveTimeSlots = timeSlots;
        }
        else if(slotStoredInLocalStorage && slotStoredInLocalStorage.length > 0){
            effectiveTimeSlots = slotStoredInLocalStorage;
        }
        else {
            effectiveTimeSlots = [];
        }
        window.localStorage.setItem('allTimeSlots', JSON.stringify(effectiveTimeSlots));
    }

    handleDateSelection = (event) => {
        const { fetchTimeSlots } = this.props;
        const selectedDate = event.currentTarget.value;

        window.localStorage.setItem('selectedDate', selectedDate);

        fetchTimeSlots(selectedDate);

        this.setState({
            selectedDate: selectedDate
        })
    }

    handleCheckboxChange = (event) => {
        this.setState(prevState => {
            return {
                checkboxChecked: !prevState.checkboxChecked
            }
        });
    }

    showTimeSlotComponent = (effectiveTimeslots) => {
        if(window.localStorage.getItem('checkboxStatus') === 'true'){
            const filteredSlots = effectiveTimeslots.filter((slot) => {
                return slot.inHomeAvailable === true
            })

            return (
                <TimeSlotsComponent
                    handleTimeSlotSelection={this.handleTimeSlotSelection}
                    timeSlots={filteredSlots}
                    clearSelectedSlot={this.state.clearSelectedSlot}
                    onSlotsReset={this.onSlotsReset}
                />
            );
        }  
        else{
            const slotStoredInLocalStorage = JSON.parse(window.localStorage.getItem('allTimeSlots'));
            const {timeSlots} = this.props;

            let effectiveTimeSlots = [];

            if(timeSlots && timeSlots.length > 0){
                effectiveTimeSlots = timeSlots;
            }
            else if(slotStoredInLocalStorage && slotStoredInLocalStorage.length > 0){
                effectiveTimeSlots = slotStoredInLocalStorage;
            }
            else {
                effectiveTimeSlots = [];
            }

            return (
                <TimeSlotsComponent
                    handleTimeSlotSelection={this.handleTimeSlotSelection}
                    timeSlots={effectiveTimeSlots}
                    clearSelectedSlot={this.state.clearSelectedSlot}
                    onSlotsReset={this.onSlotsReset}
                />
            );
        }
    }

    handleTimeSlotSelection = (selectedSlotID) => {
        debugger;
        const {timeSlots} = this.props;
        const effectiveTimeSlots = timeSlots.length > 0 ? timeSlots : JSON.parse(window.localStorage.getItem('timeSlots'));
        const result = effectiveTimeSlots.find(slot => slot.deliveryTimeId === selectedSlotID);

        this.setState({
            selectedSlot: selectedSlotID
        });
        window.localStorage.setItem('selectedTimeSlot', JSON.stringify(result));
        console.log(`Selected slot is ${JSON.stringify(result)}`)
    }

    resetTimeSlots = () => {
        this.setState({
            clearSelectedSlot: true,
            selectedSlot: undefined
        })
    }

    onSlotsReset = () => {
        this.setState({
            clearSelectedSlot: false
        });
    }

    confirmSelection = () => {
        const {history} = this.props;

        history.push('/confirm');
    }

    render() {
        const { deliveryDates, errorMessage, timeSlots } = this.props;
        const { checkboxChecked, selectedDate, selectedSlot } = this.state;
        const slotStoredInLocalStorage = JSON.parse(window.localStorage.getItem('timeSlots'));
        const allSlotStoredInLocalStorage = JSON.parse(window.localStorage.getItem('allTimeSlots'));
        let effectiveTimeslots = []

        if(timeSlots && timeSlots.length > 0){
            effectiveTimeslots = timeSlots;
        }
        else if(allSlotStoredInLocalStorage && allSlotStoredInLocalStorage.length > 0){
            effectiveTimeslots = allSlotStoredInLocalStorage;
        }
        else if(slotStoredInLocalStorage && slotStoredInLocalStorage.length > 0){
            effectiveTimeslots = slotStoredInLocalStorage;
        }
        else {
            effectiveTimeslots = [];
        }

        console.log(`selectedSlot: ${selectedSlot}`);

        return (
            <>
                <div className='row'>
                    <DeliveryDate 
                        deliveryDates={deliveryDates}
                        errorMessage={errorMessage}
                        checkboxChecked={checkboxChecked}
                        selectedDate={selectedDate}
                        handleCheckboxChange={this.handleCheckboxChange}
                        handleDateSelection={this.handleDateSelection}
                    />
                </div>

                <div className='row'>
                    <div className='col-sm-12'>
                        {this.showTimeSlotComponent(effectiveTimeslots)}
                    </div>
                </div>

                {effectiveTimeslots.length > 0 && <div className='row'>
                    <div className='col-sm-12'>
                        <Button 
                            variant="primary"
                            disabled={!selectedSlot}
                            onClick={this.confirmSelection}
                        >
                            Proceed
                        </Button>
                        <Button 
                            variant="primary"
                            disabled={!selectedSlot}
                            onClick={this.resetTimeSlots}
                        >
                            Reset
                        </Button>
                    </div>
                </div>}
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return ({
        deliveryDates: state.dateComponentReducer.deliveryDates,
        timeSlots: state.dateComponentReducer.timeSlots,
        errorMessage: state.dateComponentReducer.errorMessage
    });
}

const mapDispatchToProps = (dispatch) => {
    return ({
        fetchDeliveryDates: () => dispatch(fetchDeliveryDates()),
        fetchTimeSlots: dateString => dispatch(fetchTimeSlots(dateString))
    });
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MainComponent));