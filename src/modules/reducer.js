import { DATE_ACTIONS } from "./actions";

const INITIAL_STATE = {
    loading: false,
    deliveryDates: [],
    timeSlots: [],
    errorMessage: ''
};

export default function DateComponentReducer(state = INITIAL_STATE, action) {
    switch(action.type){
        case DATE_ACTIONS.FETCH_DATES: {
            return {
                ...state,
                loading: true,
                errorMessage: ''
            }
        }
        case DATE_ACTIONS.FETCH_DATES_SUCCESS: {
            return {
                ...state,
                loading: false,
                deliveryDates: action.payload.data,
                errorMessage: ''
            }
        }
        case DATE_ACTIONS.FETCH_DATES_FAIL: {
            return {
                ...state,
                loading: false,
                deliveryDates: [],
                errorMessage: 'Unable to fetch dates'
            }
        }
        case DATE_ACTIONS.FETCH_TIME_SLOTS: {
            return {
                ...state,
                loading: true,
                errorMessage: ''
            }
        }
        case DATE_ACTIONS.FETCH_TIME_SLOTS_SUCCESS: {
            return {
                ...state,
                loading: false,
                timeSlots: action.payload.data,
                errorMessage: ''
            }
        }
        case DATE_ACTIONS.FETCH_TIME_SLOTS_FAIL: {
            return {
                ...state,
                loading: false,
                timeSlots: [],
                errorMessage: 'Unable to fetch time slots'
            }
        }
        default: {
            return INITIAL_STATE
        }
    }
}