import React from "react";

function SuccessScreen() {
    const selectedTimeSlot = JSON.parse(window.localStorage.getItem('selectedTimeSlot'));

    return(
        <div className = 'row'>
            <div className='col-sm-12'>
                <h4>
                    {`
                        Congratulations. Your product will be delivered on 
                        ${selectedTimeSlot.deliveryDate} 
                        between ${selectedTimeSlot.startTime} and ${selectedTimeSlot.stopTime}
                    `}
                </h4>
            </div>
        </div>
    );
}

export default SuccessScreen;