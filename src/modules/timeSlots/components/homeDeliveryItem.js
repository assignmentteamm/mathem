import React from "react";

function HomeDeliveryItem(props) {
    const {inHomeAvailable} = props;
    const cssClass = inHomeAvailable ? 'text-success' : 'text-danger';

    return(
        <>
            <div className={cssClass}>
                In Home Delivery {inHomeAvailable ? 'Possible' : 'Not Possible'}
            </div>
        </>
    );
}

export default HomeDeliveryItem