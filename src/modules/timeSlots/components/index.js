import React, { Component } from "react";
import generateHash from 'random-hash';

import TimeSlotsItem from "./timeSlotsItem";

class TimeSlotsComponent extends Component {
    state = {
        selectedSlot: '',
        clearSelectedSlot: false
    }

    static getDerivedStateFromProps(props, state) {
        if (props.clearSelectedSlot !== state.clearSelectedSlot) {
            return {
                clearSelectedSlot: props.clearSelectedSlot
            }
        }

        if(props.timeSlots !== state.timeSlots){
            return {
                timeSlots: props.timeSlots
            }
        }

        return null;
    }

    componentDidUpdate() {
        this.resetSlots();
    }

    sortTimeSlots = (timeSlots) => {
        const sortedSlots = timeSlots.sort((firstSlot, nextSlot) => (firstSlot.startTime > nextSlot.startTime) ? 1 : -1);

        return sortedSlots;
    }     

    resetSlots = () => {
        const { clearSelectedSlot } = this.state;
        const { onSlotsReset } = this.props;

        if (clearSelectedSlot) {
            this.setState({
                selectedSlot: ''
            });
            onSlotsReset();
        }
    }

    handleSlotSelect = (deliveryTimeId) => {
        const {
            handleTimeSlotSelection
        } = this.props;

        this.setState({
            selectedSlot: deliveryTimeId
        })

        handleTimeSlotSelection(deliveryTimeId);
    }

    saveTimeslotsToLocalstorage = () => {
        const timeslotsInLocalStorage = window.localStorage.getItem('timeSlots');
        const {timeSlots} = this.props;
        const slotStoredInLocalStorage = JSON.parse(window.localStorage.getItem('timeSlots'));
        let effectiveTimeSlots = []
        
        if(timeSlots && timeSlots.length > 0){
            effectiveTimeSlots = timeSlots;
        }
        else if(slotStoredInLocalStorage && slotStoredInLocalStorage.length > 0){
            effectiveTimeSlots = slotStoredInLocalStorage;
        }
        else {
            effectiveTimeSlots = [];
        }
        

        if(timeSlots && timeSlots.length > 0 && timeslotsInLocalStorage !== this.props.timeSlots) {
            window.localStorage.setItem('timeSlots', JSON.stringify(effectiveTimeSlots));
        }
    }

    render() {
        const {
            timeSlots
        } = this.props;

        this.saveTimeslotsToLocalstorage();

        let slotStoredInLocalStorage = JSON.parse(window.localStorage.getItem('timeSlots'));
        slotStoredInLocalStorage = slotStoredInLocalStorage && slotStoredInLocalStorage.length > 0 ? slotStoredInLocalStorage : [];
        const timeSlotArray = timeSlots && timeSlots.length > 0 ? timeSlots : slotStoredInLocalStorage
        const sortedTimeSlots = this.sortTimeSlots(timeSlotArray);
        // const parsedSlot = slotStoredInLocalStorage && slotStoredInLocalStorage !== 'undefined' ? JSON.parse(slotStoredInLocalStorage) : undefined;
        
        if (sortedTimeSlots && sortedTimeSlots.length > 0) {
            return (
                <div className='timeSlots grid'>
                    {sortedTimeSlots.map(slot => {
                        const key = generateHash();
                        return <TimeSlotsItem
                            key={key}
                            timeSlot={slot}
                            selectedSlot={this.state.selectedSlot}
                            handleSlotSelect={this.handleSlotSelect}
                        />
                    })}
                </div>
            );
        }
        else{
            return(
                <></>
            );
        }
    }
}

export default TimeSlotsComponent