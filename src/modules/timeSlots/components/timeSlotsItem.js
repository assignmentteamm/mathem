import React from "react";
import HomeDeliveryItem from "./homeDeliveryItem";

function TimeSlotsItem(props) {
    const { deliveryTimeId, deliveryDate, inHomeAvailable, startTime, stopTime } = props.timeSlot;
    const { handleSlotSelect, selectedSlot } = props;

    return(
        <div onClick={() => {
            if(handleSlotSelect){
                handleSlotSelect(deliveryTimeId);
            }
        }}
            className={deliveryTimeId === selectedSlot ? 'selectedSlot' : ''}
        >
            <div className='time'>
                {startTime} to {stopTime}
            </div>
            <div className='date'>
                {deliveryDate}
            </div>
            <div>
                <HomeDeliveryItem
                    inHomeAvailable={inHomeAvailable}
                />
            </div>
        </div>
    );
}



export default TimeSlotsItem;