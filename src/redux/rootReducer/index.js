import {combineReducers} from 'redux';

import dateComponentReducer from "../../modules/reducer";

const RootReducer = combineReducers({
    dateComponentReducer
});

export default RootReducer;