import { createStore, applyMiddleware } from "redux";
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import RootReducer from "../rootReducer";

import { BASE_URL } from "../../config/endpointConfig";

const client = axios.create({
    baseURL: BASE_URL,
    responseType: 'json'
});

const middleware = [axiosMiddleware(client)];

export default createStore(
    RootReducer,
    applyMiddleware(...middleware)
);