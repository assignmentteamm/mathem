import React from "react";
import { Route, Router, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";

import MainComponent from "../modules/main/components";
import ConfirmScreen from "../modules/confirm/components";
import SuccessScreen from "../modules/success/components";

const history = createBrowserHistory();

function Routes() {
    return (
        <div className='container'>
            <Router history={history}>
                <Switch>
                    <Route exact path='/'>
                        <MainComponent />
                    </Route>
                    <Route exact path='/confirm'>
                        <ConfirmScreen />
                    </Route>
                    <Route exact path='/success'>
                        <SuccessScreen />
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default Routes;